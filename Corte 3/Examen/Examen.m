%Variables y condiciones iniciales
u0 = 0; u1 = 0; u2 = 0;
y0 = 0; y1 = 0; y2 = 0; y3 = 0;
%Entrada tipo paso
u = ones(1,10);
%Simulación
for n = 1:10
%Desplazamientos de la entrada
u2 = u1;
u1 = u0;
%Valor actual de la entrada
u0 = n*u(n);
%Desplazamientos de la salida
y3 = y2;
y2 = y1;
y1 = y0;
%Valor actual de la salida
y0 = -1*y1-0.75*y2-0.25*y3+u0+u1+u2;
y(n) = y0;
end
%Resultado
figure;
stem(y);