%Ejemplo del proceso de muestreo

close all
clear all
clc


%Frecuencia de muestreo
fs = 30;
%fs = 50;
%fs = 30;
%fs = 20;
%fs = 10;

Ts = 1/fs;

%Se�al seno
%wy = 2*pi*10 rad/s 
fy = 10 %Hz
Ty = 1/fy;

%Tiempo
t = 0:Ts:2*Ty;

y = sin(2*pi*10*t).*cos(2*pi*20*t);


figure
plot(t,y,'b',t,y,'r*')

