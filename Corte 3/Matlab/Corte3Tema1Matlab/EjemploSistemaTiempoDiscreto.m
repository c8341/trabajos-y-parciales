

%Ejemplo de un sistema de tiempo discreto

close all
clear all
clc

%Condiciones iniciales salida
y0 = 0; %y[n]
y1 = 0; %y[n-1]
y2 = 0; %y[n-2]

%Condiciones iniciales entrada
u0 = 0; %u[n]
u1 = 0; %u[n-1]

%Datos de entrada
u = ones(50,1);

for n=1:50
    
    y2 = y1;
    y1 = y0;
    
    u1 = u0;
    u0 = u(n);
    
    y0 = -0.5*y1 - 0.2*y2 + 1*u0 + 0.5*u1;
    y(n) = y0;
    
end

figure
n = 0:49;
stem(n,y)




