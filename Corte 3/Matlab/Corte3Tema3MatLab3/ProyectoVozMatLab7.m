% UNIVERSIDAD DISTRITAL FRANCISCO JOS� DE CALDAS
% AN�LISIS DE SE�ALES DISCRETAS
% PROYECTO: Identificaci�n de voz

close all
clear all

%%%%%%An�lisis de frecuencia de las se�ales%%%%%
%Lectura de la se�al 1
[y1,Fs]= wavread('Voz_Hombre.wav');

%Figura en el dominio del tiempo
figure
plot(y1)

%An�lisis de Fourier
Y1=fft(y1);

%Escala de frecuencia
N=length(y1);
K=0:N-1;
f=Fs*K/N;

%Figura del espectro en frecuencia Hz
figure
plot(f,abs(Y1))

%Figura del espectro en frecuencia Wn
Wn = 2*K/N;
figure
plot(Wn,abs(Y1))

%Lectura de la se�al 2
[y2,Fs]= wavread('Voz_Mujer.wav');

%Figura en el dominio del tiempo
figure
plot(y2)

%An�lisis de Fourier
Y2=fft(y2);

%Escala de frecuencia
N=length(y2);
K=0:N-1;
f=Fs*K/N;

%Figura del espectro en frecuencia Hz
figure
plot(f,abs(Y2))

%Figura del espectro en frecuencia Wn
Wn = 2*K/N;
figure
plot(Wn,abs(Y2))

%%%%%Dise�o del filtro f1 hombre%%%%%
[B1,A1]=butter(5,[0.2 0.5]);
G=tf(B1,A1,1/Fs)

%%%%%Dise�o del filtro f2 mujer%%%%%
[B2,A2]=butter(5,[0.3 0.6]);
G=tf(B2,A2,1/Fs)

%%%%%Prueba del Sistema%%%%%
%Se�al de entrada Masculino
%Y=y1;
%Se�al de entrada Femenino
Y=y2;

%Se�al en la salida del filtro
yf1=filter(B1,A1,Y);
yf2=filter(B2,A2,Y);

%Potencia de las se�ales filtradas
P1=sum(yf1.^2)/length(yf1);
P2=sum(yf2.^2)/length(yf2);

%Respuesta del sistema 
if P1>P2
    disp('Voz masculina')
    msgbox('Voz masculina')
else
    disp('Voz femenina')
    msgbox('Voz femenina')
end
