%Universidad Distrital FJC
%Cibern�tica II
%Transformada Z
%Dise�o e implementaci�n de filtros digitales
%Aplicaci�n: Filtrado de una se�al de voz

clear all
close all


%%%Parte 1%%%
%%%Adquisici�n de la se�al%%%

%Cargando la se�al de voz
%[x,fs,Nbits]=wavread('Ejemplo1.wav');
[x,fs]=audioread('Ejemplo1.wav');

%Reproduciendo la se�al
sound(x,fs)


%%%Parte 2%%%
%%%Dise�o del filtro digital%%%

%Dise�ando el filtro en tiempo continuo
%Orden del filtro
N=3;

%Frecuencia de corte en Hz
f=2000;

%Frecuencia de corte en rad/seg
Wn=2*pi*f;

%Filtro pasa bajos
[Bs,As]=butter(N,Wn,'low','s');

%Filtro pasa altos
%[Bs,As]=butter(N,Wn,'High','s');

%Funci�n de transferencia
Hs=tf(Bs,As)

%Respuesta en frecuencia
figure
[FHs,Ws]=freqs(Bs,As);
plot(Ws,abs(FHs))
title('Respuesta en frecuencia del filtro an�logo')


%%%Parte 3%%%
%%%Obtenci�n del filtro digital%%%

%Discretizando el sistema. M�todos:
%'imp' Invarianza al impulso  
%'tustin' Transformaci�n bilineal

%Periodo de muestreo
Ts=1/fs;

%Continuo a discreto
Hz=c2d(Hs,Ts,'tustin') 

%Recuperando los coeficientes del filtro
[Bz,Az]=tfdata(Hz,'v');

%Respuesta en frecuencia
figure
[FHz,Wz]=freqz(Bz,Az);
plot(Wz,abs(FHz))
title('Respuesta en frecuencia del filtro digital')


%%%Parte 4%%%
%Aplicaci�n del filtro digital
y=filter(Bz,Az,x);

%Reproduciendo el resultado
sound(y,fs)


%%%Parte 5%%%
%Programaci�n del filtro digital de tercer orden
n=max(size(x));
ya=zeros(n,1);

%Variables y condiciones iniciales
x3=0; x2=0; x1=0; x0=0;
y3=0; y2=0; y1=0; y0=0;

%Implementaci�n de la convoluci�n 
for i=1:n
    %Desplazamientos de la entrada 
    x3=x2;
    x2=x1;
    x1=x0;
    %Valor actual de la entrada
    x0=x(i);
    %Desplazamientos de la salida
    y3=y2;
    y2=y1;
    y1=y0;
    %C�culo de la salida
    y0=Bz(1)*x0+Bz(2)*x1+Bz(3)*x2+Bz(4)*x3-(Az(2)*y1+Az(3)*y2+Az(4)*y3);
    ya(i)=y0;
end

%Reproduciendo el resultado
sound(ya,fs)
