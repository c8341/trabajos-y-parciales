%Ejemplo de la implementacion de un sistema de tiempo discreto

clear all
close all

%Variables y condiciones iniciales
u0 = 0; u1 = 0; u2 = 0; u3 = 0;
y0 = 0; y1 = 0; y2 = 0; y3 = 0;

%Entrada tipo paso
u = ones(1,20);

%Simulación
for n = 1:20
    %Desplazamientos de la entrada
    u3 = u2;
    u2 = u1;
    u1 = u0;
    %Valor actual de la entrada
    u0 = u(n);
    %Desplazamientos de la salida
    y3 = y2;
    y2 = y1;
    y1 = y0;
    %Valor actual de la salida
    y0 = -2*y1-3*y2-4*y3+0*u0+1*u1+4*u2+3*u3;
    y(n) = y0;
end

%Resultado
figure
stem(y)